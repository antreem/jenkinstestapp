fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios install_pods
```
fastlane ios install_pods
```
Install pod dependencies
### ios unittest
```
fastlane ios unittest
```

### ios metrics
```
fastlane ios metrics
```

### ios beta
```
fastlane ios beta
```

### ios dev_certificates_fetch
```
fastlane ios dev_certificates_fetch
```
Fetches the development certificates and provisioning profiles to run the project on real devices
### ios dist_certificates_fetch
```
fastlane ios dist_certificates_fetch
```
Fetches the distribution certificates and provisioning profiles compile the app for AppStore distribution

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
