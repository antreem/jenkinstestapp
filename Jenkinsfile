pipeline {
  agent {
    node {
      label 'ios'
    }

  }
  stages {
    stage('Pipeline dependencies') {
      parallel {
        stage('Pipeline dependencies') {
          steps {
            echo "PATH is: $PATH"
            sh '''
            if [ -x "$(command -v lizard)" ]; then
              echo \'Lizard dependency is installed.\';
            else
              echo \'Error: lizard is not installed.\';
              
              if ! [ -d "${HOME}/.pyenv" ]; then 
                echo \'Error: pyenv not installed. Installing pyenv v3.8.0 ...\';
                pyenv install 3.8.0;
              fi

              pyenv local 3.8.0;
              pyenv rehash;

              curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py; python get-pip.py;
              echo \'Installing lizard...\';
              pip install lizard;
            fi
          '''
            sh 'rbenv local 2.6.5; rbenv rehash; bundle;'
            echo 'All dependency are installed.'
          }
        }
        stage('Promote to') {
          when {
            expression {
              currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause) != null
            }

          }
          steps {
            input(message: 'Promote to build', id: 'promotebuild', ok: 'Promote')
          }
        }
      }
    }
    stage('Artifactory configuration') {
      steps {
        rtServer(id: 'Artifactory-Server', url: 'http://jfrog.antreem.com/artifactory', username: 'user', password: 'Jfrog123admin')
      }
    }
    stage('Unit test') {
      steps {
        sh 'bundle exec fastlane unittest'
      }
    }
    stage('Static analyzer') {
      steps {
        sh 'bundle exec fastlane metrics'
      }
    }
    stage('Deploy - DEV') {
      steps {
        script {
          if (currentBuild.result == null
          || currentBuild.result == 'SUCCESS') {
            if (env.BRANCH_NAME ==~ /master/) {
              // Deploy when the committed branch is master (we use fastlane to complete this)
              sh 'bundle exec fastlane beta'

              rtUpload (
                serverId: 'Artifactory-Server',
                spec: '''{
"files": [
{
"pattern": "output/*.ipa",
"target": "example-repo-local/"
}
]
}''',
                )
              }
            }
          }

        }
      }
    }
    environment {
      LANG = 'it_IT.UTF-8'
      LC_ALL = 'it_IT.UTF-8'
      RBENV_ROOT = "$HOME/.rbenv"
      RBENV_SHELL = 'sh'
      PYENV_ROOT = "$HOME/.pyenv"
      PYENV_SHELL = 'sh'
      SONAR_SCANNER_ROOT = "$HOME/Workspace/sonarqube/sonar-scanner/bin"
      PATH = "$PYENV_ROOT/bin:$PYENV_ROOT/shims:$RBENV_ROOT/bin:$RBENV_ROOT/shims:/usr/local/bin:$SONAR_SCANNER_ROOT:$PATH"
      MATCH_PASSWORD = 'ANT123pwd'
    }
    post {
      always {
        archiveArtifacts(allowEmptyArchive: true, artifacts: 'output/*.ipa')
        junit 'reports/report.junit'

      }

    }
  }